<?php namespace Empu\TawkTo\Components;

use Cms\Classes\ComponentBase;
use Empu\TawkTo\Models\Widget;

class WidgetScript extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'TawkTo Widget Script',
            'description' => 'Add widget code to the html'
        ];
    }

    public function defineProperties()
    {
        return [
            'widget' => [
                'title'             => 'Select widget',
                'description'       => 'Widget code will be embedded',
                'type'              => 'dropdown'
            ]
        ];
    }

    public function onRun()
    {
        $id =  $this->property('widget');
        $this->page['tawktoWidgetScript'] = Widget::find($id);
    }

    public function getWidgetOptions()
    {
        return Widget::lists('name', 'id');
    }

}
