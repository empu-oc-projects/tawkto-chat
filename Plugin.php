<?php namespace Empu\TawkTo;

use Backend;
use System\Classes\PluginBase;

/**
 * TawkTo Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Tawk.to',
            'description' => 'Twak.to live chat widget manager',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-comments-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Empu\TawkTo\Components\WidgetScript' => 'widgetScript',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'empu.tawkto.access_settings' => [
                'tab' => 'Tawk.to',
                'label' => 'Change tawk.to settings'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'tawkto' => [
                'label'       => 'TawkTo',
                'url'         => Backend::url('empu/tawkto/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.tawkto.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'widgets' => [
                'label'       => 'Tawk.to',
                'description' => 'Manage tawk.to widgets.',
                'icon'        => 'icon-comments-o',
                'category' => 'Marketing',
                'url'         => Backend::url('empu/tawkto/widgets'),
                'order'       => 500,
                'keywords'    => 'chat tawkto widget',
                'permissions' => ['empu.tawkto.access_settings']
            ]
        ];
    }

}
