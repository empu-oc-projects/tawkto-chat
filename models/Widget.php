<?php namespace Empu\TawkTo\Models;

use Model;

/**
 * Widget Model
 */
class Widget extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'empu_tawkto_widgets';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'code'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
