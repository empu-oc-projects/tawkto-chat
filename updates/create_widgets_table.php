<?php namespace Empu\TawkTo\Updates;

use Schema;
use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWidgetsTable extends Migration
{
    public function up()
    {
        Schema::create('empu_tawkto_widgets', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('code');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_tawkto_widgets');
    }
}
